# Redis Demo

Redis and Redis Commander containers can be created using the `docker-compose.yml` file in the solution root.  
The Redis URL is `jdbc:redis://localhost:6379/0`. This URL can be used in Rider, for example, for easily checking
the cache contents.  
Redis Commander can be used from this URL: [http://localhost:9090](http://localhost:9090).

## Session data from ASP.NET Core

This tutorial was used: https://requestmetrics.com/building/episode-16-using-redis-for-distributed-user-sessions-in-asp-net-core  
You need the [Microsoft.Extensions.Caching.StackExchangeRedis](https://www.nuget.org/packages/Microsoft.Extensions.Caching.StackExchangeRedis/8.0.4)
NuGet package.  
You can test the setup by pointing your browser to the following URLs (you can also check the contents of your Redis instance).
  * http://localhost:5000/write-to-session
  * http://localhost:5000/read-from-session

## Redis backplane for SignalR

This tutorial was used to set up SignalR: https://learn.microsoft.com/en-us/aspnet/core/tutorials/signalr?view=aspnetcore-8.0  
This tutorial was used to enable Redis: https://learn.microsoft.com/en-us/aspnet/core/signalr/redis-backplane?view=aspnetcore-8.0  
You need the [Microsoft.AspNetCore.SignalR.StackExchangeRedis](https://www.nuget.org/packages/Microsoft.AspNetCore.SignalR.StackExchangeRedis/8.0.4)
NuGet package.

[SignalR will be powered by Redis' publish/subscribe mechanism](https://learn.microsoft.com/en-us/aspnet/signalr/overview/performance/scaleout-in-signalr).  
The SignalR connection data won't be stored as cache entries in the same way that the session data is stored. Instead, the connections will be
managed by Redis.  
To see the actual connections, you can open a Redis console and execute the following command:
```redis
PUBSUB CHANNELS *
```
